import HomeControler from '../src/components/homeCtrl';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  `<div id="body--container" class="body--container"></div>
   <div id="tarifa" class="tarifa"></div>
   <div id="subtarifa" class="subtarifa"></div>
    <select id="lang">
        <option value="es">Español</option>
        <option value="en">English</option>
    </select>
    `;

});

test('Test home component', () => {
    let home = [{
        activo: true,caja_derecha_texto: "<p>Texto prueba</p>", caja_derecha_titulo: "Texto prueba",
        caja_izquierda_texto: "<p>Texto prueba</p>", caja_izquierda_titulo: "Texto prueba",
        idioma: 1, lang: "en", pk: 1, subtitulo: "Texto prueba", titulo: "Texto prueba"
    }];
        
    let tarifas = {
    results: [{
        activo: true,
        codtarifa: 1,  destacado: true,  logo: "http://localhost:8000/media/Logo/loconejo.png",
        nombretarifa: "CONEJO", precio: 7, pretitulo: "Conejo", slug: "conejo",
        subtarifas: [{subtarifa_cent_minuto: 0.15, subtarifa_datos_internet: 6,
            subtarifa_est_llamada: 0, subtarifa_id: 1, subtarifa_minutos_gratis: 200,
            subtarifa_minutos_ilimitados: false, subtarifa_num_canales: null,
            subtarifa_omv: null, subtarifa_precio_sms: null, subtarifa_siglas_omv: "Cnjo",
            subtarifa_velocidad_conexion_bajada: null, subtarifa_velocidad_conexion_subida: null,
            tipo_tarifa: 1, subtarifa_tarifa: [{ codtarifa: 1 }] 
            }]
        }]
    };

    let lang = {
        footer:{
            btocotact: "SEND QUERY",
            cookies: "Cookies",
            legal: "Legal warning",
            tabout: "ABOUT COMPANY",
            tlocation: "OUR LOCATION",
            tsocial: "SOCIAL LINKS"},
        home:{
            tarifa:{
                fiber: {text: "Down speed", speed: "MB/s"},
                landline: {minutes: "€/Min", fminutes: "FREE minutes"},
                phone: {speed: "GB at 4G speed", minutes: "€/Min", fminutes: "FREE minutes"},
                tv: {channels: "TV Chanels"},
                wifi: {text: "Down speed", speed: "MB/s"}
            }
        },
        lang: "en"}

    let datos_empresa = {
        textos: [{
            key: "jumbotron-1", content: "", image: "http://localhost:8000/media/info_empresa_image/jumbotron-1.jpg", lang: "es"
        }]};
    
    new HomeControler(home, tarifas, datos_empresa, lang);

    expect($('#body--container').length).toBeGreaterThan(0);
    expect($('#ofertas').length).toBeGreaterThan(0);

});