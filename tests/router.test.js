import {Router} from '../src/router.js'; 

it('Router test', () => {
     
    Router.add(/cookies/);
    expect(Router.routes.length).toBe(1);
    
    Router.add(/avisolegal/);
    expect(Router.routes.length).toBe(2);
    
    Router.remove(/avisolegal/);
    expect(Router.routes.length).toBe(1);
    
    Router.flush();
    expect(Router.routes.length).toBe(0);
});