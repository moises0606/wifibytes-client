import CookiesControler from '../src/components/cookiesCtrl';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="body--container" class="body--container"></div>';
  
});

test('Test cookies component', () => {
    let data = {
        content: '<p>Texto de prueba</p>', 
        image: null, key: "cookies", 
        lang: "es",
        textos: [{
            content: "Texto prueba",
            image: null,
            key: "cookies",
            lang: "en"
        }]
    };
    let lang = {
        footer:{
            btocotact: "SEND QUERY",
            cookies: "Cookies",
            legal: "Legal warning",
            tabout: "ABOUT COMPANY",
            tlocation: "OUR LOCATION",
            tsocial: "SOCIAL LINKS"},
        home:{
            tarifa:{
                fiber: {text: "Down speed", speed: "MB/s"},
                landline: {minutes: "€/Min", fminutes: "FREE minutes"},
                phone: {speed: "GB at 4G speed", minutes: "€/Min", fminutes: "FREE minutes"},
                tv: {channels: "TV Chanels"},
                wifi: {text: "Down speed", speed: "MB/s"}
            }
        },
        lang: "en"}

    new CookiesControler(data, lang);
    expect($('#body--container').length).toBeGreaterThan(0);
});

