import FooterController from '../src/components/layout/footerCtrl';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="footer" class="footer"></div>';
  
});

test('Test footer component', () => {
    let data = {
        aboutus: {
            key: "AboutUs", 
            content: "Texto prueba", 
            image: null, 
            lang: "es"
        },
        address: "Texto prueba",
        lang: "es",
        cifnif: "Texto prueba",
        city: "Texto prueba",
        country: "Texto prueba",
        facebook: "Texto prueba",
        icon_logo: null,
        location_lat: "Texto prueba",
        location_long: "Texto prueba",
        logo: "Texto prueba",
        name: "Texto prueba",
        phone: "Texto prueba",
        province: "Texto prueba",
        twitter: "Texto prueba",
        zipcode: "Texto prueba",
        textos: [{
            content: "Texto Prueba",
            image: null,
            key: "AboutUs",
            lang: "en"
        }]
    };

    let lang = {
        footer:{
            btocotact: "SEND QUERY",
            cookies: "Cookies",
            legal: "Legal warning",
            tabout: "ABOUT COMPANY",
            tlocation: "OUR LOCATION",
            tsocial: "SOCIAL LINKS"},
        home:{
            tarifa:{
                fiber: {text: "Down speed", speed: "MB/s"},
                landline: {minutes: "€/Min", fminutes: "FREE minutes"},
                phone: {speed: "GB at 4G speed", minutes: "€/Min", fminutes: "FREE minutes"},
                tv: {channels: "TV Chanels"},
                wifi: {text: "Down speed", speed: "MB/s"}
            }
        },
        lang: "en"}
    new FooterController(data, lang);
    expect($('#footer').length).toBeGreaterThan(0);
});


