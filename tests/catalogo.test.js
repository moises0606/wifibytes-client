import CatalogoComponent from '../src/components/catalogo';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="body--container" class="body--container"></div>';
  
});

test('Test catalogo component', () => {
    let data = {
        results: 
            [{
                activo: true,
                camara: 1,
                codfamilia: "tlf",
                codimpuesto: "IVA21%",
                descripcion: "Nuevo Telefono",
                descripcion_breve: "Telefono",
                descripcion_larga: "<p>Especificaciones de prueba</p>",
                destacado: false,
                imagen: "Test",
                thumbnail: "Test",
                visible: true
            }]
        
    };
    let lang = {
        footer:{
            btocotact: "SEND QUERY",
            cookies: "Cookies",
            legal: "Legal warning",
            tabout: "ABOUT COMPANY",
            tlocation: "OUR LOCATION",
            tsocial: "SOCIAL LINKS"},
        home:{
            tarifa:{
                fiber: {text: "Down speed", speed: "MB/s"},
                landline: {minutes: "€/Min", fminutes: "FREE minutes"},
                phone: {speed: "GB at 4G speed", minutes: "€/Min", fminutes: "FREE minutes"},
                tv: {channels: "TV Chanels"},
                wifi: {text: "Down speed", speed: "MB/s"}
            }
        },
        lang: "en"}

    new CatalogoComponent(data, lang);
    expect($('#body--container').length).toBeGreaterThan(0);
});

