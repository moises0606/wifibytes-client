import {setlang} from '../src/services/lang.js';
import {getLang} from '../src/lib/languages';

const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  `<select id="lang" >
    <option value="es">Español</option>
    <option value="en">English</option>
  </select>
  `;
  
});

let obj = {
  "lang": "en",
  "footer": {
      "tabout":"ABOUT COMPANY",
      "tsocial":"SOCIAL LINKS",
      "tlocation":"OUR LOCATION",
      "btocotact":"SEND QUERY",
      "legal":"Legal warning",
      "cookies":"Cookies"
  },
  "home": {
      "tarifa":{
          "phone": {
              "speed":"GB at 4G speed",
              "minutes": "€/Min",
              "fminutes": "FREE minutes"
          },
          "landline": {
              "minutes": "€/Min",
              "fminutes": "FREE minutes"
          },
          "fiber": {
              "text": "Down speed",
              "speed": "MB/s"
          },
          "wifi": {
              "text": "Down speed",
              "speed": "MB/s"
          },
          "tv": {
              "channels": "TV Chanels"
          }
      }
  }
}

test('Test lang service', () => {
    expect(setlang('es')).toBe(true);
    expect(getLang('en')).toEqual(obj);
});