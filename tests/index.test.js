import {langvalue} from '../src/index.js';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  `<select id="lang" >
    <option value="es">Español</option>
    <option value="en">English</option>
  </select>
  `;
  
});

test('Test index function, when change lang', () => {
    expect(langvalue()).toBe(true);
});