import {Utils} from '../src/lib/utils';

const $ = require('jquery');
beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="body--container" class="body--container"></div>';
  
});

test('Test cookies', () => {
    Utils.setCookie('testing', 'testing', 365)
    expect(Utils.getCookie('testing')).toBe("testing");
});
