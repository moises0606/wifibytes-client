
/**
 * @class
 */
class CookiesComponent {

    constructor(datos_empresaJSON, lang) {
        this.datos_empresaJSON = datos_empresaJSON;
        this.lang = lang;
        document.getElementById("body--container").innerHTML = this.render();
    }
  
    /** render  */
    render() {
        /* Recogemos de textos empresa el texto sobre cookies y lo mostramos en la pagina web */
            let lang = this.lang;
            let empresa = this.datos_empresaJSON;

            /* Devolvemos el texto que sea la clave cookies y el idioma sea el seleccionado */
            const cookies = empresa.textos.filter((texto) => texto.key == 'cookies' && texto.lang == lang.lang);

            /* Hacemos un map para coger el el objeto y se lo rellenamos para 
            posteriormente mostrarlo en el html devolviendoselo al constructor*/
            return cookies.map((cookies) => {
                return `
                    <div class="cookies">
                    ${cookies.content}
                    </div>`;
            })
    }
}
export default CookiesComponent;