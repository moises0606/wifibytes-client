/**
 * @class
 */
class AvisoLegalComponent {

    constructor(datos_empresaJSON, lang) {
        this.datos_empresaJSON = datos_empresaJSON;
        this.lang = lang;
        document.getElementById("body--container").innerHTML = this.render();
    }
  
    /** render  */
    render() {
        /* Recogemos de textos empresa el aviso legal y lo mostramos en la pagina web */
            let lang = this.lang;
            let empresa = this.datos_empresaJSON;
            /* Devolvemos el texto que sea la clave avisolegal y el idioma sea el seleccionado */
            const avisolegal = empresa.textos.filter((texto) => texto.key == 'avisolegal' && texto.lang == lang.lang);
            
            /* Hacemos un map para coger el el objeto y rellenamos el html para 
            posteriormente mostrarlo */
            return avisolegal.map((avisolegal) => {
                return `    
                    <div class="avisolegal">
                        ${avisolegal.content}
                    </div>`;
            })
    }
}
export default AvisoLegalComponent;