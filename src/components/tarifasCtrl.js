/**
 * @class
 */
class TarifasComponent {

    constructor(tarifaJSON, lang) {
        this.tarifaJSON = tarifaJSON;
        this.lang = lang;
        document.getElementById("body--container").innerHTML ="";
        this.render();
    }
  
    /** render  */
    render() {
            let lang = this.lang;
            let tarifas = this.tarifaJSON.results;

            /* Hacemos un map para coger el el objeto y rellenamos el html para 
            posteriormente mostrarlo */
            document.getElementById("body--container").innerHTML = "<div id='tarifas--body'</div>";

            tarifas.map((tarifa) => {
                tarifa.nombretarifa = tarifa.nombretarifa.toUpperCase();
                /**
                 *  Rellenamos el html y lo mostramos en el html en la etiqueta ofertas
                 */
                //let html = template_tarifas(obj);
                document.getElementById("tarifas--body").innerHTML += `    
                <div class="tarifa--tarifa">
                    <div class="tarifa--header"><img src=${tarifa.logo} height="42" width="42">
                        <h1>${tarifa.nombretarifa}</h1></div>
                    <div class="tarifa--precio"><h1>${tarifa.precio}€/MES</h1></div>
                    <div id="#${tarifa.codtarifa}"class="tarifa--footer">
                    
                    </div>
                </div>`;
                
                tarifa.subtarifas.map((subtarifa) => {
                    /**
                     *  Cada subtarifa es enviada a la funcion subtarifas, donde le 
                     * hacemos un switch y devolvemos la subtarifa con el html y las 
                     * acomulamos en el footer de la tarifa que lleva el id de la misma
                     */
                    let html = subtarifas(subtarifa);
                    document.getElementById("#"+tarifa.codtarifa).innerHTML += html;   
                })

                function subtarifas(subtarifa){
                    switch(subtarifa.tipo_tarifa) {
                    case 1: //Movil
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_datos_internet} ${lang.home.tarifa.phone.speed}</p>
                                    <p>${subtarifa.subtarifa_cent_minuto} ${lang.home.tarifa.phone.minutes}<p>
                                    <p>${subtarifa.subtarifa_minutos_gratis} ${lang.home.tarifa.phone.fminutes}<p>
                                </div>`;
                        break;
                    case 2: //Fijo
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_cent_minuto} ${lang.home.tarifa.landline.minutes}<p>
                                    <p>${subtarifa.subtarifa_minutos_gratis} ${lang.home.tarifa.landline.fminutes}<p>
                                </div>`;
                        break;
                    case 3: //Fibra
                        return `<div class="subtarifa">
                                    <p>${lang.home.tarifa.fiber.text} ${subtarifa.subtarifa_velocidad_conexion_bajada} ${lang.home.tarifa.fiber.speed}<p>
                                    <p>${lang.home.tarifa.fiber.text} ${subtarifa.subtarifa_velocidad_conexion_subida} ${lang.home.tarifa.fiber.speed}<p>
                                </div>`;
                        break;
                    case 4: //Wifi
                        return `<div class="subtarifa">
                                    <p>${lang.home.tarifa.wifi.text} ${subtarifa.subtarifa_velocidad_conexion_bajada} ${lang.home.tarifa.wifi.speed}<p>
                                    <p>${lang.home.tarifa.wifi.text} ${subtarifa.subtarifa_velocidad_conexion_subida} ${lang.home.tarifa.wifi.speed}<p>
                                </div>`;
                        break;
                    case 5: //TV
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_num_canales} ${lang.home.tarifa.tv.channels}<p>
                                </div>`;
                        break;
                    };
                }
                
            }) 
    }
}
export default TarifasComponent;