/**
 * @class
 */
class MainComponent {

    constructor() {
        document.getElementById("main").innerHTML = this.render();
    }
  
    /** render  */
    render() {
        /* Le devolvemos al constructor el html para que lo inyecte en arbol DOM */
            return `
            <div class="menu--desplegable">
                <label for="menu-toggle">
                    <div class="boton">
                      <span></span>
                      <span></span>
                      <span></span>
                    </div>
                </label>  
                <input type="checkbox" id="menu-toggle"/>
                <ul id="menu">
                  <li><a href="#" class="menu--option" id="home">Home</a></li>
                  <li><a href="#tarifas" class="menu--option" id="tarifas">Tarifas</a></li>
                  <li><a href="#catalogo" class="menu--option" id="catalogo">Catalogo</a></li>
                  <li><a href="#contact" class="menu--option" id="contact">Contact</a></li>
                  </ul>
            </div>
            <div>
                <select id="lang">
                    <option value="es">Español</option>
                    <option value="en">English</option>
                </select>
            </div>`;
    }
}
export default MainComponent;