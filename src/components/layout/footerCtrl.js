/**
 * @class
 */
class FooterComponent {

    constructor(datos_empresaJSON, lang) {
        this.datos_empresaJSON = datos_empresaJSON;
        this.lang = lang;
        document.getElementById("footer").innerHTML = this.render();
    }
  
    /** render  */
    render() {
        let lang = this.lang;

        let empresa = this.datos_empresaJSON;
            /* Recogemos el texto que mostramos en el footer dependiendo del idioma
            despues lo volvemos a inyectar en empresa*/
            const textos = empresa.textos.filter((empresa) => {
                return empresa.key == 'AboutUs' && empresa.lang == lang.lang;
            });
            empresa.aboutus = textos[0];

            return `
            <section id="footer-sec" >
                    <div class="container">
                <div class="row  pad-bottom" >
                    <div class="col">
                        <h4> <strong>${lang.footer.tabout}</strong> </h4>
                                    <p>
                                        ${empresa.aboutus.content}
                                    </p>
                        <a href="#contact" >read more</a>
                        </div>
                    <div class="col">
                            <h4> <strong>${lang.footer.tsocial}</strong> </h4>
                        <p>
                            <a href=${empresa.facebook}><i class="fa fa-facebook-square fa-3x"  ></i></a>
                            <a href="${empresa.twitter}"><i class="fa fa-twitter-square fa-3x"  ></i></a>
                            <!-- <a href="#"><i class="fa fa-linkedin-square fa-3x"  ></i></a>
                            <a href="#"><i class="fa fa-google-plus-square fa-3x"  ></i></a> -->
                        </p>
                        </div>
                    <div class="col">
                        <h4> <strong>${lang.footer.tlocation}</strong> </h4>
                                    <p>
                                    ${empresa.address}<br />
                                    ${empresa.city}, ${empresa.province}<br />
                                    ${empresa.country} - ${empresa.zipcode}
                                    </p>

                            <a href="#contact" class="btn btn-primary" >SEND QUERY</a>
                        </div>
                    </div>
                    </div>
            </section>
            <section id="footer-sec2" >
                <div class="container">
                <a href="#avisolegal" class="footer-sec2-links">${lang.footer.legal}</a>
                <a href="#cookies" class="footer-sec2-links">${lang.footer.cookies}</a>
                </div>
            </section>`;
    }
}
export default FooterComponent;