/**
 * @class
 */
class HomeComponent {

    constructor(homeJSON, tarifasJSON, datos_empresaJSON, lang) {
        this.homeJSON = homeJSON;
        this.tarifasJSON = tarifasJSON;
        this.datos_empresaJSON = datos_empresaJSON;
        this.lang = lang;
        document.getElementById("body--container").innerHTML = this.home();
        this.tarifas();
        this.jumbotron();
    }
  
    /** render  */
    home() {
        /* Recoge los datos de la empresa y los muestra dependiendo del idioma seleccionado
        en la barra de menu, por defecto, es en español */
            let lang = this.lang;

            /**
             * Filtramos el json para seleccionar el home en el idioma deseado
             */
            let Empresa = this.homeJSON.filter((Empresa) => Empresa.lang == lang.lang)
            Empresa = Empresa[0];

            /**
             * Mostramos en el html el home (se lo devolvemos al constructor y este lo 
             * muestra en el html)
             */
            return  `
            <h1 id=titulo>${Empresa.titulo}</h1>
            <h2 id=subtitulo>${Empresa.subtitulo}</h2>
            <div id="jumbotron">
                    <div class="jumbotron">
                            <input checked type=radio name="slider" id="slide1" />
                            <input type=radio name="slider" id="slide2" />
                            <input type=radio name="slider" id="slide3" />
                            <input type=radio name="slider" id="slide4" />
                            <input type=radio name="slider" id="slide5" />
                            
                            <div class="slider-jumbotron">
                                <div id="inner" class=inner>
                                    
                                </div>
                                <!-- .inner -->
                            </div>
                            <!-- .slider-jumbotron -->
                            
                            <div class="slider-prev-next-control">
                                <label for=slide1></label>
                                <label for=slide2></label>
                                <label for=slide3></label>
                                <label for=slide4></label>
                            </div>
                            <!-- .slider-prev-next-control -->
                            
                            <div class="slider-dot-control">
                                <label for=slide1></label>
                                <label for=slide2></label>
                                <label for=slide3></label>
                                <label for=slide4></label>
                            </div>
                            <!-- .slider-dot-control -->
                        </div>
            </div>
            <div id="ofertas">
            </div>
            <div class=container--textos>
                <div class=caja>
                    <h1 id=caja_izquierda_titulo>${Empresa.caja_izquierda_titulo}</h1>
                    <p id=caja_izquierda_texto>${Empresa.caja_izquierda_texto}</p>
                </div>
                <div class=caja>
                    <h1 id=caja_derecha_titulo>${Empresa.caja_derecha_titulo}</h1>
                    <p id=caja_derecha_texto>${Empresa.caja_derecha_texto}</p>
                </div>
            </div>
    `;
    }
        
        tarifas() {
            let lang = this.lang;

            /* Recoge las tarifas destacadas y las muestra en el home utilizando 
            el sistena de templates */
            let tarifas = this.tarifasJSON;

            /* Busca las tarifas destacadas */
            const tarifa = tarifas.results.filter((tarifas) => {
                return tarifas.destacado == true;
            });

            tarifa.map((tarifa) => {
                tarifa.nombretarifa = tarifa.nombretarifa.toUpperCase();
                /**
                 *  Rellenamos el html y lo mostramos en el html en la etiqueta ofertas
                 */
                //let html = template_tarifas(obj);
                document.getElementById("ofertas").innerHTML += `    
                <div class="tarifa--container">
                    <div class="tarifa--header"><img src=${tarifa.logo} height="42" width="42">
                        <h1>${tarifa.nombretarifa}</h1></div>
                    <div class="tarifa--precio"><h1>${tarifa.precio}€/MES</h1></div>
                    <div id="#${tarifa.codtarifa}"class="tarifa--footer">
                    
                    </div>
                </div>`;
                
                tarifa.subtarifas.map((subtarifa) => {
                    /**
                     *  Cada subtarifa es enviada a la funcion subtarifas, donde le 
                     * hacemos un switch y devolvemos la subtarifa con el html y las 
                     * acomulamos en el footer de la tarifa que lleva el id de la misma
                     */
                    let html = subtarifas(subtarifa);
                    document.getElementById("#"+tarifa.codtarifa).innerHTML += html;   
                })

                function subtarifas(subtarifa){
                    switch(subtarifa.tipo_tarifa) {
                    case 1: //Movil
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_datos_internet} ${lang.home.tarifa.phone.speed}</p>
                                    <p>${subtarifa.subtarifa_cent_minuto} ${lang.home.tarifa.phone.minutes}<p>
                                    <p>${subtarifa.subtarifa_minutos_gratis} ${lang.home.tarifa.phone.fminutes}<p>
                                </div>`;
                        break;
                    case 2: //Fijo
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_cent_minuto} ${lang.home.tarifa.landline.minutes}<p>
                                    <p>${subtarifa.subtarifa_minutos_gratis} ${lang.home.tarifa.landline.fminutes}<p>
                                </div>`;
                        break;
                    case 3: //Fibra
                        return `<div class="subtarifa">
                                    <p>${lang.home.tarifa.fiber.text} ${subtarifa.subtarifa_velocidad_conexion_bajada} ${lang.home.tarifa.fiber.speed}<p>
                                    <p>${lang.home.tarifa.fiber.text} ${subtarifa.subtarifa_velocidad_conexion_subida} ${lang.home.tarifa.fiber.speed}<p>
                                </div>`;
                        break;
                    case 4: //Wifi
                        return `<div class="subtarifa">
                                    <p>${lang.home.tarifa.wifi.text} ${subtarifa.subtarifa_velocidad_conexion_bajada} ${lang.home.tarifa.wifi.speed}<p>
                                    <p>${lang.home.tarifa.wifi.text} ${subtarifa.subtarifa_velocidad_conexion_subida} ${lang.home.tarifa.wifi.speed}<p>
                                </div>`;
                        break;
                    case 5: //TV
                        return `<div class="subtarifa">
                                    <p>${subtarifa.subtarifa_num_canales} ${lang.home.tarifa.tv.channels}<p>
                                </div>`;
                        break;
                    };
                }
                
            }) 
        }

        jumbotron() {
        /* Recoge los datos de la empresa para buscar las imagenes que mostraremos en el jumbotron */
            let empresa = this.datos_empresaJSON;
            /* Devolvemos a jumbotronimg todos los objetos que contienen en la llave jumbotron */ 
            const jumbotronimg = empresa.textos.filter((texto) => /jumbotron/.test(texto.key));

            jumbotronimg.map((img) => {
                /* Rellenamos el html que inyectamos a través del arbol DOM en inner */
                document.getElementById("inner").innerHTML += `        
                <article>
                    <img src=${img.image} />
                </article>`;
            })
        }


    
}
export default HomeComponent;
