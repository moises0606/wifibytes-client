
/**
 * @class
 */
class CatalogoComponent {

    constructor(articuloJSON, lang) {
        this.articuloJSON = articuloJSON.results;
        this.lang = lang;
        document.getElementById("body--container").innerHTML = "";
        this.render();
    }
  
    /** render  */
    render() {
            let lang = this.lang;
            let articulo = this.articuloJSON;

            /**
             * Recogemos los articulos que estan activados para posteriormente
             * realizar un map y mostrarlos en el html 
             */
            const articulos = articulo.filter((articulo) => articulo.activo == true);

            articulos.map((articulo) => {
                document.getElementById("body--container").innerHTML += 
                `   
                <div class="articulos">
                    <div class="articulo">
                        <img src="${articulo.imagen}">
                        ${articulo.descripcion}
                    </div>
                </div>`;
            })
    }
}
export default CatalogoComponent;