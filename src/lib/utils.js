import {Settings} from '../settings';

/** Ajax call 
 * @param url endpoint
 * @param callback function to be executed when we get a real answer from server. This callback always get the real response as parameter
*/

let CACHE = new Map();

/**
 * Realizamos un objeto con todas las funciones de utils para no tener que 
 * exportar e importar cada una de ellas, asi solo exportamos e importamos una 
 * y ya esta
 */
let Utils = {
    get: function(url,callback){
        /* Si el endpoint ya se ha llamado, retornará los datos de este endpoint ya que estarán
        guardados en la cache, en caso contrario realizará la query */
        if (CACHE.has(Settings.baseURL+url)) {
            callback(CACHE.get(Settings.baseURL+url));
        }else{
            var xhr = new XMLHttpRequest();
            xhr.open('GET', Settings.baseURL+url);
            
            xhr.onload = () => {
                if (xhr.status === 200) {
                    /* Guardamos en la cache el endpoint y los datos */
                    CACHE.set(Settings.baseURL+url,xhr.response);
                    callback(xhr.response);
                }else {
                    alert('Request failed.  Returned status of ' + this.status);
                }
            };
            xhr.send();
        }
    },

    setCookie: function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },

    getCookie: function(cname) {
        let regex = new RegExp(cname+"[\s]*=[\s]*([\w])*");
        return document.cookie.match(regex).input.split('=')[1];
    }
}

export {Utils};


