import spanish from "./i18n/spanish.json";
import english from "./i18n/english.json";

/**
 * Realizamos un objeto de los json
 */
const getLangs = {  
    'en': english,
    'es': spanish 
};

/**
 * 
 * @param {*} lang
 * Recivimos el idioma que ha seleccionado el usuario (estará en cookies)
 * y devolvemos el json del idioma que el usuario tiene guardado en cookies,
 * si no recivimos nada porque no esta en cookies, por defecto nos devolvera
 * el json en ingles
 */
let getLang = function(lang) {
    switch(lang) {
        case "es":        
            return getLangs.es;
        case "en":
            return getLangs.en;
        default:
            return getLangs.en;
    }
}

export {getLang};