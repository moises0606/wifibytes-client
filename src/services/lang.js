import HomeComponent from '../components/homeCtrl';
import ContactComponent from '../components/contactCtrl';
import FooterComponent from '../components/layout/footerCtrl.js';
import AvisoLegalComponent from '../components/avisolegalCtrl.js';
import CookiesComponent from '../components/cookiesCtrl.js';
import TarifaComponent from '../components/tarifasCtrl.js';
import CatalogoComponent from '../components/catalogo.js';
import {Router} from '../router.js'; //Knows what to do for every single URL 
import {Utils} from '../lib/utils';

/**
 * En este servicio se guarda en localStorage el idioma y 
 * a traves del get.fragment el se podrá volver a lanzar el controlador donde se encuentra
 * el usuario
 * @function
 */
let setlang = function(lang) {
    /* Recogemos el valor del select del menu en el que decidimos el idioma y 
    lo guardamos en cookies */
    Utils.setCookie('lang', lang.lang, 365);

    /* Dependiendo de donde este el usuario cargaremos un controlador o otro
    para evitar redirecciones, así se actualizará en tiempo real el controlador en 
    el que esta el usuario, ya que en cada uno de ellos cuando comprueba que mostrar en la 
    web comprobará el idioma comparando en el localStorage.lang */
    switch(Router.getFragment()) {
        case "contact":        
            Utils.get('/datos_empresa/?format=json',function(response){
                let datos_empresaJSON = JSON.parse(response);
                new ContactComponent(datos_empresaJSON, lang);
                new FooterComponent(datos_empresaJSON, lang);
            });            
            break;
        case "cookies":
            Utils.get('/datos_empresa/?format=json',function(response){
                let datos_empresaJSON = JSON.parse(response);
                new CookiesComponent(datos_empresaJSON, lang);
                new FooterComponent(datos_empresaJSON, lang);
            });            
            break;
        case "avisolegal":
            Utils.get('/datos_empresa/?format=json',function(response){
                let datos_empresaJSON = JSON.parse(response);
                new AvisoLegalComponent(datos_empresaJSON, lang);
                new FooterComponent(datos_empresaJSON, lang);
            });            
            break;
        case "tarifas":
            Utils.get('/tarifa/?format=json',function(response){
                let tarifaJSON = JSON.parse(response);
                new TarifaComponent(tarifaJSON, lang);
                new FooterComponent(datos_empresaJSON, lang);
            });
            break;
        case "catalogo":
            Utils.get('/articulo/?format=json',function(response){
                let articuloJSON = JSON.parse(response);
                new CatalogoComponent(articuloJSON, lang);
                new FooterComponent(datos_empresaJSON, lang);
            });
            break;
        default:
            Utils.get('/home/?format=json',function(response){
                let homeJSON = JSON.parse(response);
                Utils.get('/tarifa/?format=json',function(response){
                    let tarifasJSON = JSON.parse(response);
                    Utils.get('/datos_empresa/?format=json',function(response){
                        let datos_empresaJSON = JSON.parse(response);
                        new HomeComponent(homeJSON, tarifasJSON, datos_empresaJSON, lang);
                        new FooterComponent(datos_empresaJSON, lang);
                    });
                });
            });
            /* Ponemos este return para cuando hacemos los tests comprobar que funciona
            y saber si ha podido completar la operacion */
            return true;
            break;
    }
}

export {setlang};