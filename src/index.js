import {Router} from './router.js'; //Knows what to do for every single URL 
import HomeComponent from './components/homeCtrl';
import ContactComponent from './components/contactCtrl';
import MainComponent from './components/layout/mainCtrl.js';
import FooterComponent from './components/layout/footerCtrl.js';
import AvisoLegalComponent from './components/avisolegalCtrl.js';
import CookiesComponent from './components/cookiesCtrl.js';
import TarifaComponent from './components/tarifasCtrl.js';
import CatalogoComponent from './components/catalogo.js';
import {setlang} from './services/lang.js'
import {Settings} from './settings';
import {Utils} from './lib/utils';
import {getLang} from "./lib/languages";

/* Dependiendo de la url cargaremos un controlador distinto, por defecto es el home */
Router
.add(/contact/, function() {
    console.log("Contact");
    Utils.get('/datos_empresa/?format=json',function(response){
        let datos_empresaJSON = JSON.parse(response);
        new ContactComponent(datos_empresaJSON, getLang(Utils.getCookie('lang')));
    });
;}).listen()
.add(/avisolegal/, function() {
    console.log("AvisoLegal");
    Utils.get('/datos_empresa/?format=json',function(response){
        let datos_empresaJSON = JSON.parse(response);
        new AvisoLegalComponent(datos_empresaJSON, getLang(Utils.getCookie('lang')));
    });
}).listen()
.add(/cookies/, function() {
    console.log("Cookies");
    Utils.get('/datos_empresa/?format=json',function(response){
        let datos_empresaJSON = JSON.parse(response);
        new CookiesComponent(datos_empresaJSON, getLang(Utils.getCookie('lang')));
    });
}).listen()
.add(/tarifas/, function() {
    console.log("Tarifas");
    Utils.get('/tarifa/?format=json',function(response){
        let tarifaJSON = JSON.parse(response);
        new TarifaComponent(tarifaJSON, getLang(Utils.getCookie('lang')));
    });
}).listen()
.add(/catalogo/, function() {
    console.log("Catalogo");
    Utils.get('/articulo/?format=json',function(response){
        let articuloJSON = JSON.parse(response);
        new CatalogoComponent(articuloJSON, getLang(Utils.getCookie('lang')));
    });
}).listen()
.add(function() {
    console.log('default_(home)');
    Utils.get('/home/?format=json',function(response){
        let homeJSON = JSON.parse(response);
        Utils.get('/tarifa/?format=json',function(response){
            let tarifasJSON = JSON.parse(response);
            Utils.get('/datos_empresa/?format=json',function(response){
                let datos_empresaJSON = JSON.parse(response);
                new HomeComponent(homeJSON, tarifasJSON, datos_empresaJSON, getLang(Utils.getCookie('lang')));
            });
        });
    });
});

/* Cargamos los componentes automaticos, son los que nos mostrará por defecto al cargar la web */
document.addEventListener("DOMContentLoaded",function(){
    new MainComponent(getLang(Utils.getCookie('lang')));
    Utils.get('/home/?format=json',function(response){
        let homeJSON = JSON.parse(response);
        Utils.get('/tarifa/?format=json',function(response){
            let tarifasJSON = JSON.parse(response);
            Utils.get('/datos_empresa/?format=json',function(response){
                let datos_empresaJSON = JSON.parse(response);
                new HomeComponent(homeJSON, tarifasJSON, datos_empresaJSON, getLang(Utils.getCookie('lang')));
                new FooterComponent(datos_empresaJSON, getLang(Utils.getCookie('lang')));
            });
        });
    });
});

/* Desde aqui esperamos a que cambie el evento del idioma, cuando cambia, renderiazmos 
el home, para que vuelva a cargar el home en el idioma indicado */
document.addEventListener("DOMContentLoaded",function(){
    let select = document.getElementById("lang");
    /* Cuando cambia el idioma se llama a la funcion langvalue */
    select.addEventListener("change", function(){langvalue(undefined, Utils.getCookie('lang'))});
    /* Al entrar a la web se llamará por defecto a la funcion langvalue enviando
    el parametro first */
    langvalue('first', Utils.getCookie('lang'));
});

/**
 * Esta funcion se llama cuando se accede por primera vez a la web o 
 * cuando el usuario cambia el idioma
 * @param {string} aux 
 * @function
 */
let langvalue = function (aux = undefined, lang = undefined) {
    let select = document.getElementById("lang");

    /* Si se acaba de iniciar la sesion lo sabremos por el parametro first y si el 
    cookies es diferente a undefined se actualiza el valor en del "select" del
    menu en el que decidimos el idioma*/
    if ((lang != undefined) && (aux == 'first')) {
        select.value = lang;
    }else {
        /* Cuando entramos a esta parte es porque el usuario ha cambiado el idioma i necesita 
        actualizarlo tanto en cookies y en el valor del menu asi como cambiar el idioma 
        de la web */
        setlang(getLang(select.value));
        return true;
    }
}

export {langvalue};